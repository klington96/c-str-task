﻿#include <iostream>
#include <string>

int main()
{
    std::cout << "Enter a word: ";
	std::string word;
    std::cin >> word;

	std::cout << word << "\n";
	std::cout << "Length of word:" << word.length() << "\n";
	std::cout << "First symbol:" << word[0] << "\n";
	std::cout << "Last symbol:" << word[word.length() - 1] << "\n";
}

